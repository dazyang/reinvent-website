---
layout: page
title: Progress
permalink: /progress/
---

This page contains a simple weekly progress log, so you can follow along what is happening and we keep things open and transparent.

- I: indicates a general item relating to the Institute as a whole
- M: indicates an item relating to the Makerspace
- R: indicates an item relating to Report

## Week of 18 February 2019
- Received a new cool logo for Re:INVENT from a member in the Open Design Report mailing list.
- Sent an e-mail to The Hague University of Applied Sciences for exploring collaboration.
- Put a student of The Hague University of Applied Sciences looking for a graduation project into contact with Latra.
- Sent e-mails to various people and communities, asking if they consider themselves a "Humanitarian Maker".
- Sent out a new Doodle for planning the second Designers With Refugees meetup.
- Continued conversation in Designers With Refugees group.

## Week of 11 February 2019
- Participated in Humanitarian Makers Builders Call.
- Updated the [Community page](/community) to reflect current connections and collaborations.

## Week of 4 February 2019
- Had a call (Problem Interview) with MadeInShoes.
- Participated in a meet-up organized by Latra on a new humanitarian design initiative: Designers With Refugees.

## Week of 28 January 2019
- Attended "The Hague International Organisation & NGO Reception 2019" to meet people of INGOs.
- Rescheduled calls with MadeInshoes and with Humanitarian Makers.
- Responded to several organizations on their proposals for the open Call for Challenges for Humanitarian Design.
- Continued e-mail conversation with Humanitarian Makers.
- Visted Fab Lab Mariahoeve amongst others for follow-up of the Humanitarian Makers Meetup in December.

## Week of 21 January 2019
- Had a call with someone at [Digitial Society School](https://digitalsocietyschool.org/) about potential collaboration.
- Attended "The Future of African Cities" expert roundtable session of The Spindle.
- Participated in the second Public Call of Humanitarian Makers.
- Reached out to Rural Spark, BRCK and Wisdom Stoves about the open Call for Challenges for Humanitarian Design.
- Initiated a low-profile Design Thinking process for Humanitarian Makers.

## Week of 14 January 2019
- Had a meeting with teachers of Rotterdam University about providing humanitarian design challenges for industrial product design students to work on.
- Sent out a [Call for Challenges](http://think-innovation.com/blog/open-call-for-humanitarian-design-challenges/) to a wide range of Humanitarian NGO community bodies and on Social Media.
- Participated in a Humanitarian Makers Public Call, to have a dialogue about the future direction with other community members.
- Participated in an online video call with the Infrastructure Working Group of the MakerNet Alliance, to explore which digital communication tools can support the ongoing work of the Alliance.
- Participated in an online video call with designers interested in working on humanitarian design for [Latra](https://www.latra.gr/).
- Created a template for online collaboration on Empathy Maps for Humanitarian Makers using OnlyOffice, as alternative for Google Docs. 
- Created a room on Riot.im for direct messaging between the Latra designers, as alternative for Slack.

## Week of 7 January 2019
- Participated in the DIN SPEC OSH Legal and Definition conference calls.
- Had extensive call with Humanitarian Makers on how to move them forward in 2019.
- Continued dialogue with Latra about potential collaboration.
- Started dialogue with Made In Shoes about potential collaboration.
- Reviewed draft logo for Re:INVENT by Indo.

## Week of 31 December 2018
- Contacted [Waterscope](https://www.waterscope.org/), [Made In Shoes](https://www.wevolver.com/jonathan.rushton/made.in.shoes/master/blob/documentation.md) and [Glia Project](https://github.com/GliaX/Otoscope) offering help on testing or otherwise from the Fab Lab Mariahoeve community.
- Continued dialogue with Humanitarian Makers on joint efforts.
- Reviewed draft documents of the DIN SPEC Open Source Hardware initiative.
- Had a phone call with [Latra](https://www.latra.gr/) about potential collaboration.

## Week of 17 and 24 December 2018
No activity due to holidays.

## Week of 10 December 2018
- Organized Humanitarian Makers Meetup at Fab Lab Mariahoeve in The Hague, The Netherlands

## Week of 3 December 2018
- I: Participated in a meeting with Mercy Corps.
- I: Participated in 2nd conference call about the DIN SPEC Open Source Hardware standardization.
- R: Had a conference call with a representative of Open Source Ecology Germany about their OSH search engine initiative.
- I: Arranged upcoming Meetup at FabLab Mariahoeve with tele-presence of a representative of [Humanitarian Makers](https://www.humanitarianmakers.org/).
- I: Continued dialogue on Affordable High-Tech for Humanitarian Aid on Telegram

## Week of 26 November 2018
- I: Participated in 2nd conference call about Affordable High-Tech for Humanitarian Aid, which is actually more about "Humanitarian Makers" in general.
- I: Gave a [presentation and workshop on Open Innovation and Open Design](https://thespindle.org/2018/12/03/future-session-10-open-innovation-open-design/) for The Spindle at The Hague Humanity Hub for amongst others Oxfam, Terre des Hommes and CHOICE.
- I: Talked with a representative of Wunderpeople and Rotterdam University of Applied Sciences about a possible collaboration for a 'Dutch Makers for Frugal Innovation' program.
- I: Continued dialogue on Affordable High-Tech for Humanitarian Aid on Telegram

## week of 19 November 2018
- I: Continued dialogue in the [Fairchat](https://fairchat.net/home) channel about the DIN SPEC Open Source Hardware standardization initiative in Germany.
- I: Continued working on the Future Session for The Spindle.
- I: Scheduled 2nd conference call with people from Fab Labs and Humanitarian Makers about Affordable High-Tech for Humanitarian Aid.
- R: Reached out to people at TU Berlin working on the Open Source Hardware Observatory 2.0.

## Week of 12 November 2018
- R: Sent update after pitching the report initiative at the [REMODEL Conference](https://danskdesigncenter.dk/en/designing-open-business-day-celebrating-open-source-based-business-development) to the [odmag mailing list](https://lists.ghserv.net/mailman/listinfo/odmag).
- I: Continued dialogue in the [Fairchat](https://fairchat.net/home) channel about the DIN SPEC Open Source Hardware standardization initiative in Germany.
- I: Talked to a representative of the MakerNet Alliance about a possible collaboration.
- I: Talked to a representative of BRCK.com about a potential collaboration.
- I: Started working on a [presentation and workshop](http://thespindle.org/event/future-session-10/) for The Spindle about "tech trends" with Open Design as major theme.

## Week of 5 November 2018
- R: Connected to Open Design experts at the [REMODEL Conference](https://danskdesigncenter.dk/en/events/double-event-designing-open-business-design-delivers) of Danish Design Center and agreed on a joint initiative for a new edition of the [Journal of Peer Production](http://peerproduction.net/), new version of the [Observatory](https://opensourcedesign.cc/observatory/), report and new book with working title "Open Design Still" as a follow-up from [Open Design Now](http://opendesignnow.org/).
- I: Rescheduled call with BRCK.com.
- I: Rescheduled call about Affordable High-Tech for Humanitarian Aid.
- I: Rescheduled call with MakerNet Alliance.
- M: Tried contacting Wunderpeople about the Making Initiative in The Hague.

## Week of 29 October 2018
- I: Reviewed plan of Humanitarian Makers about Affordable High-Tech for Humanitarian Aid.
- I: Scheduled call with small group of people from Fab Labs and Humanitarian Makers about Affordable High-Tech for Humanitarian Aid.
- I: Scheduled call with [BRCK.com](https://www.brck.com/) about potential IoT challenge.
- I: Scheduled call with representative of [MakerNet Alliance](http://www.makernetalliance.org/).
- M: Tried contacting Wunderpeople about the Making Initiative in The Hague.

## Week of 24 October 2018
- R: Finished post with [my ideas on the Report]({% post_url 2018-10-24-ideas-on-the-report %}).
- M: Contacted [RDM Makerspace](https://www.rdmmakerspace.nl/) about participating in a local Making Initiative.
- I: Cross-posted the Reinventing Startups post [on think-innovation.com](http://think-innovation.com/blog/reinventing-startups/) and shared on Mastodon and Twitter.
- R: Communicated with hosting provider Greenhost about fixing the broken odmag mailing list.

## Week of 15 October 2018
- M: Made telephone calls and exchanged e-mails with Humanity Hub and Wunderpeople about the Making Initiative in The Hague.
- I: Continued dialogue on Affordable High-Tech for Humanitarian Aid on Telegram, now waiting for a Humanitarian Maker for first version of plan/approach.
- R: Started writing post with braindump of my ideas on the Report.

## Week of 8 October 2018
- M: Received 3D Printer parts from China: electronics, heated bed and solder iron
- M: Participated in meeting with representatives of The Hague Humanity Hub and Wunderpeople for a "Making initiative" in The Hague aimed at Humanitarian work.
- I: Continued dialogue with Fab Lab community members and a representative of Humanitarian Makers about the EU's Affordable High-Tech for Humanitarian Aid prizes.
- I: Wrote post about [reinventing startups]({% post_url 2018-10-10-reinventing-startups %}) and started a thread on the [Zebras Unite forum](https://zebrasunite.mn.co/) asking for feedback.

## Week of 1 October 2018
- I: Finished 3D Printing giveaways, wrote [post about them]({% post_url 2018-10-01-giveaways-for-impact-startup-fest %}) and made connections at Impact Startup Fest
- I: Sent link about EU's [EUR 5 million for Affordable High-Tech for Humanitarian Aid](https://ec.europa.eu/research/eic/index.cfm?pg=prizes_aid) to various networks to explore opportunities together
- I: Created Fab Lab Network side channel on Telegram to explore the Affordable High-Tech for Humanitarian Aid challenge, [join the channel](https://t.me/joinchat/Bs2qqxIveg7KiCAEyfYWwA) (you need Telegram)
- I: Added list of example Open Design initiatives to website as inspiration

## Week of 24 September 2018

- M: Rescheduled meeting with Humanity Hub about their ideas for a 'Humanitarian' Makerspace, now set for 10 October
- I: Updated the reinvent.cc website
- I: Preparing presence at [Impact Startup Fest](https://impactstartupfest.com/) 2 October with a 3D Printed giveaway
- M: Had a dialogue with the creator of LaserDuo about a build workshop

## Week of 17 September 2018

- M: Continued purchasing parts for the first 3D Printer: aluminium profile and electronic parts
- I: Migrated the reinvent.cc website from GitHub Pages to GitLab Pages
- I: Struggled a lot getting TLS to work on GitLab Pages, completed with help from FSFE NL
- R: Sent an e-mail do Danish Design Center asking to add as a proposal to the [REMODEL](https://danskdesigncenter.dk/en/understand-open-source-manufacturing-30-minutes) Conference early November
- R: Continued dialogue in the [Mailing List](https://lists.ghserv.net/mailman/listinfo/odmag)
- M: Continued research for potential open designs

## Week of 10 September 2018

- I: Set up first version of the reinvent.cc website using GitHub Pages and Jekyll
- I: Set up [Taiga](https://tree.taiga.io/project/diderik-reinvent/kanban), for keeping track of work items
- M: Researched potential designs for a CNC Mill and Lasercutter

## Week of 3 September 2018

- I: Presented the Re:INVENT initiative for the first time, at [The Hague Humanity Hub](http://humanityhub.org/) Membership Orientation

