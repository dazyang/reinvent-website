---
layout: default
---

#  Reinventing the Future. Together.
A collaborative initiative to advance Open Design for creating a Safe and Just Space for Humanity.

## Mission
Our mission is to bring the power of technology, product design and fabrication in the hands of the people, so every community on the planet has the ability to create their own livelihood.

## Vision
Our goal is that by the end of 2020 we will have a group of passionate 'hands-on' experts of various disciplines spending part of their time collaboratively working on practical projects from our own initiative and from partner organizations asking for help on real-world problems, from the perspective of Open Design. 

## Strategy
Re:INVENT takes a freedom, transparent, open, cooperative, meritrocal and community approach. This means that in principal any idea, concept, project or program is possible, as long as it has a foundation in Open Design and advances empowerment in providing basic human needs.

## Re:INVENT_Institute
The Re:INVENT Institute is the umbrella organization from which various interrelated programs and projects are started.

At the moment we are [connecting to various initiatives and communities](/community) to figure out the role and added value Re:INVENT can have in the larger ecosystem. See the [Progress page](/progress) for weekly updates.

This website was created using GitLab Pages and Jekyll.

Latest update: see [GitLab repository](https://gitlab.com/re-invent/reinvent-website)
