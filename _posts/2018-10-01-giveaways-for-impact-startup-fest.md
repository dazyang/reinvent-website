---
layout: post
title: "Giveaways for Impact Startup Fest"
categories: 
---

1 October 2018
By: Diderik van Wingerden

On 2 October 2018 another edition of [Impact Startup Fest](https://impactstartupfest.com/) will take place in The Hague.

Last year I gave an interactive presentation there called ["Scale Up Your Impact By Going Open Source"](https://www.slideshare.net/DiderikvanWingerden/scale-up-your-impact-by-going-open-source-impact-startup-fest).

This year I decided keep it uncomplicated and just walk around, go to interesting sessions and meet amazing people. But how nice would it be to bring something as a 'conversation starter'? Hmm... not that uncomplicated after all.

At the previous [3D Cafe of FabLab Mariahoeve](https://www.meetup.com/FabLab-Mariahoeve/photos/29231109/474364297/) we built a Creality Ender 3 3D Printer. After the meetup the volunteers there tested the build with various designs, amongst others this nice little cute Touring Bike Business Card.

Would this bike also print well on my 3D Printers at home? I did some test prints and before I knew it: was printing a bunch of them to take to Impact Startup Fest.

So, here they are:

![Bike Giveaways](/media/bikes-cards.jpg)

Maybe you are reading this because you met me at the Fest and have one of the 3D Printed bikes. Good for you! If not, print one yourself from [the design on Thingiverse](https://www.thingiverse.com/thing:714484), or make an appointment so we can print one together! (Remark: this is more of a 'contact us' request for a nice conversation of shared interests, than that I intend my home to become a 3D Printing Factory)

About the [Touring Bike Business Card](https://www.thingiverse.com/thing:714484): 

The design was made by [Katsuhiko Kikkawa](https://www.thingiverse.com/CyberCyclist/about) from Japan. The designer published the design under the [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/). This means that anyone can use this design in any possible way, also commercially, without even referencing/attributing the designer. 

Are you looking for how to assemble the bike? Here are the steps:

![Touring Bike Business Card Assembly Steps](/media/bike-assembly-steps.jpg)