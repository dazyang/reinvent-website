---
layout: post
title: "If we share everyone wins."
categories: report
---

10 December 2018
By: Diderik van Wingerden

The text below is a part of [the vacancy for the Digital Society School](https://digitalsocietyschool.org/insight/senior-track-associate-trainer-researcher-within-the-systems-for-sharing-track/) (DSS), created by Amsterdam University of Applied Sciences (AUAS), Faculty of Digital Media and Creative Industries. As the text will probably disappear once the vacancy is closed or filled and because I found the text very well formulated, I decided to copy-and-paste the text here.

Once of the biggest challenges for a broad adoption of the principles of Open Source, Open Design and The Commons is a narrative that speaks to a broad audience. A story that inspires like Capitalism, Communism or Socialism once did, but without being another "ism" with its know limitations and faults. A new kind of inspiring and appealing story. The only thing missing from the text was the "sharing of knowledge and data", but that was easily corrected!

So, here is the (slightly altered) text:

## "If we share, everyone wins." 

The slogan of the Creative Commons initiative is a fine illustration of the power of sharing, commons and a focus on a society for all.

Some of the most urgent challenges the world faces today are part of complex social systems and include inequality, racism, social isolation, health hazards and climate change. Not to mention that billions of people are often still unable to meet their basic needs for food, water and housing.

More and more Social Systems communities, organizations, and local governments are taking action to address these challenges by exercising a basic human practice: sharing. By sharing their labor, space, goods, knowledge, data and more, people are overcoming scarcity by building and maintaining vital common resources. They show that sharing can lead everyone to have more, together.

In the complex digital society that has evolved in the past decades, sharing may seem to have become easier by the rise of digital systems. However, in recent years we have also all experienced that issues such as trust, privacy, undemocratic power divide us and that greed has been in the way. There is a need for fairer, more responsible and accountable systems (both online and offline) that facilitate the commons and a culture of sharing.

In this Track we will work on technical, digital and social systems. We create proof of concepts, prototypes and do applied research to support ’systems for sharing’ in the light of the UN’s Sustainable Development Goals. We will also train organizations and professionals to adopt a different mindset and be able to stimulate a sharing culture within their work environment or community.