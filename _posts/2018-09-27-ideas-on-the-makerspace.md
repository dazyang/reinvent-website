---
layout: post
title: "Ideas on the Makerspace"
categories: makerspace
---

27 September 2018
By: Diderik van Wingerden

This post contains a 'braindump' of thoughts on the Re:INVENT Makerspace.

## Role of the Makerspace

If the goal of Re:INVENT is to advance Open Design for providing in basic human needs, then one practical thing we could do is make physical things ourselves. These can be things for ourselves, for others, to learn from, to improve and/or demonstrate. These can be things from our own designs, from the designs of others or from a combination of both.

If we are to make physical things, then we need a place where we can go that is suited to make things and the tools to make these things. 

If we are to adopt the concept of 'sharing global, making local', allowing easy and consistent replication of things at scale, then we need computer-controlled machines besides traditional tools. Think of the typical computer-controlled machines found in a modern FabLab and Makerspace: a couple of 3D Printers, a Lasercutter, a CNC Mill. Perhaps a few other machines.

If we are to 'practice what we preach', then these computer-controlled machines should be Open Design themselves, demonstrating that it is possible to DIY make the very machines that are used to make the things. 

If we believe that the power to make things should be in the hands of the many, than these machines should be of good quality, physically safe and be usable with limited training and the Makerspace should in principle be open to anyone.

If we are to demonstrate that making the machines and the things are also possible in more resource-constrained parts of the world, then the machines and things should be buildable using relatively cheap, locally sourced and standard components.

If the goal of the Makerspace is to make things that can be made elsewhere as well, then it makes sense to opt for "lowest common denominator" machines regarding performance, size, accuracy and functionality. It would not make sense to use a very exotic (and hence not much used and probably expensive) machine to demonstrate something can be made, knowing that few would be able to replicate it. Furthermore, it would not make sense to have machines that can create things that are (much) more detailed than needed for creating functional things or use little-used exotic materials: "perfect is the enemy of good (enough)".

**This is the idea of the "Open Source" Makerspace: a Makerspace consisting of only Open Source DIY good-enough-quality machines that can be built using relatively cheap, locally sourced and standard components.**

Following from a [dialogue on the Fab Labs Forum](https://discuss.fablabs.io/t/which-machines-for-an-open-source-fablab/8690):

## Requirements for the machines

Overall requirements for the machines:
- 100% Open Design
- Standard, locally sourcable components as much as possible
- Robust, dependable, safe, proven, easy to use designs/machines 
- Low in expected maintenance/calibration/repair work
- Good price/quality ratio (“perfect is the enemy of good”)
- Good documentation: for the build, use, troubleshooting, etc.
- An established and active community (no abandoned projects)
- Local (entrepreneurial) community members who can support in the build and maintenance

For each type of machine:

## 3D Printers

Since 3D Printers do not produce things fast (relative to other machines), it would make sense to have several of them (say 2 to 4). 


, these are the potentials found so far:



